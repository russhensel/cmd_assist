#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jan  8 21:02:33 2022

@author: russ
just to make main easy to find, also at 
bottom of many files


"""


if __name__ == "__main__":

    # ------------------------------------------

    #----- run the full app
    import cmd_assist
    cmd_assist.main()
    # app  = clip_board.App( None, None )

# =================== eof ==============================