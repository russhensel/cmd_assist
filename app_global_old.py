# -*- coding: utf-8 -*-


"""
Created on Sun Aug  6 21:06:50 2017

@author: Russ

for clipboard
typical use:
    from app_global import AppGlobal

    self.parameters          = AppGlobal.parameters
    AppGlobal.parameters     = self


"""


# import sys
# import webbrowser
# from   subprocess import Popen
# from   pathlib import Path
# import os
# import psutil
# from   tkinter import messagebox
# import logging





# ===============================
class AppGlobal( object ):
    """
    manages parameter values for all of Smart Terminal app and its various data logging
    and monitoring applications
    generally available to most of the application through the Controllers instance variable
    """
    cmd_processor           = None   # injected by command processor
    controller              = None
    parameters              = None

    logger_id               = None
    #scheduled_event_list    = "None"
    #helper                  = "None"

    force_log_level         = 99    # value to force logging, high but not for errors
    fll                     = force_log_level   # shorthand

    # gui                     = None      # populated by the gui

    #helper                  = None      # populated externally by...
    #helper_thread_id        = None      # set by run in helper thread
    main_thread_id          = None
    logger_id               = None      # populated by the controller
    scheduled_event_list    = None      # populated externally by... -- think dead


#    live_graph_opt          = 1         # 1 = all in helper                this is just an experiment

    text_editors            = [   r"D:\apps\Notepad++\notepad++.exe", r"C:\apps\Notepad++\notepad++.exe",
                                  r"gedit", r"xed", r"leafpad"   ]   # or init from parameters or put best guess first

    text_editors            = [   r"gedit", r"xed", r"D:\apps\Notepad++\notepad++.exe", r"C:\apps\Notepad++\notepad++.exe",
                                  r"gedit", r"xed", r"leafpad", r"mousepad"  ]


    # -----------------------
    def __init__(self,  controller  ):
        """
        use as singleton as a class never instance
        """
        y=1/0



# ==============================================
if __name__ == '__main__':
    """
    run the app here for convenience of launching
    """
    print( "" )
    print( " ========== starting ScheduleMe from sch_me.py ==============" )
#    import sch_me
#    a_app = sch_me.ScheduleMe(  )

    print( AppGlobal.state_info( ) )

# ======================== eof ======================


