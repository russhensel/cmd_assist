Application Purpose:

    GUI application to issue commands for the linux terminal so that you do not 
    have to remember the details of the commands.  
    Can also be used as a application launcher.

Status:
    
    Generally works.  Coverage of linux very incomplete.  Repository is for developers
    to download and run as if a work in progress, no pip install..... 

Some Features:

    * Search for commands based on key words.
    * Commands come in sets which are logically grouped.
    * Commands are fully editable.
    * Commands are issued to the terminal ( or to preview, or to clipboard, or file ).

Some Command Details:

    * Commands are organized both around the purpose ( like list files ) and
      syntax ( like ls )

    * Commands are broken into several parts each of which is editable, and may
      have a comment.  The parts appear in dropdown lists for easy selection.

    * When commands are run the parts are assembled, comments removed, and the
      full command is passed to a terminal to be run.

Run the Code.
    
    * Download the repository into your python environment ( I use conda/spyder )
    * Run main.py and install dependencies until errors end.
          ( know ones include: pyperclip ... )
 
Some Remarks on the code:

    * App does not have a specification so it evolves as ideas occur and is actually used.
      Some ideas are tried and abandoned
    * Code is populated with dubious comments and dead or incomplete code.  but
      app mostly works.
    * I usually treat code as a learning experience in python so the code reflects that.
    * I often refactor, but only as time an inclination allow.  Plans tend to
      evolve faster than I can implement.
    * About 250K of .py files
    * Work most needed restricted to files of about 30k, high similarity with
      existing code.
    * I format with much more white space then pep8 and like it, any new work
      could be done with a similar formatting or, say, black.  I do not want to
      reformat old code unless it needs to be substantially changed.
    * Tests are good, but currently I do not use very much.
    * I am a duck typer, not a type hints sort of coder.

Contributing:

    * I am open to any help the is helpful.  Which means?
    * A start might be to extend to new linux commands and functional areas.  I
      usually work on it as I am in the process of doing something new ( to me )
      in Linux ( nothing recently, windows is my most often used os ).
    * I do not really use git for control, just keep the code there.  This
      could change.
    * A good place to start would be with a new command module ( similar to commands_1.py
      say commands_8.py ).
    * I can help you get started, we could meet on zoom.
    * Addition of tests if that is your interest.
    * Extend to work on windows with powershell?.

    * Contributions not currently important ( to me).
        * Reformatting
        * Type hints ( but fine in new code )
        * Minor changes that do not extend the application.

See also the Wiki, here.
